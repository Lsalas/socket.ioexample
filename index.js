const path= require('path');
const express = require('express');
const app = express();
//configuraciones
app.set('port', process.env.PORT || 3000);// configurar el puerto 

//archivos estáticos
app.use(express.static(path.join(__dirname,'public')));

//iniciar el servidor
const server = app.listen(app.get('port'),() =>{
    console.log('servidor on port'+ app.get('port'));
    });

const SocketIO = require('socket.io');
const io =SocketIO(server);


//websockets
io.on('connection',(socket)=>{
    console.log('New connection');
    //on evento de escuchar
    //emit evento de emitir
    socket.on('chat:message',(data)=>{
        io.sockets.emit('chat:message',data);
    });
    socket.on('chat:typing',(data)=>{
        //cuando quiero emitir a todos excepto a mi 
        socket.broadcast.emit('chat:typing',data);

    });
});
